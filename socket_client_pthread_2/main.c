/**
 * \file main.c
 * \brief fichier main qui lance la première fenêtre de jeu 
 *
 * \author tom LARRAUFIE, kevin ZHOU, issam TEBBIKH
 * \version 0.1
 * \date 20/12/2019

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>
//#include "glade.h"

#include "clientcxnmanager.h"
#include "featureConfig.h"
#include "featureConfig.h"
#include <gtk/gtk.h>



GtkBuilder *builder = NULL;

/*
 * 
 */


/**
 * Lance la première fenêtre glade 
 * @param argc
 * @param argv
 * @return 
 */
int main(int argc, char** argv) {
    
    

    gtk_init(&argc, &argv);

    /* Création de la fenêtre 1*/
    GtkWidget *win;
    builder = gtk_builder_new_from_file("glade/win.glade");
    win = GTK_WIDGET(gtk_builder_get_object(builder, "win"));
    gtk_builder_connect_signals(builder, NULL); //lance la fenetre
    gtk_widget_show(win); //lance l'application
    
    /* Affichage et boucle évènementielle */
    gtk_main();
    
    return (EXIT_SUCCESS);
}




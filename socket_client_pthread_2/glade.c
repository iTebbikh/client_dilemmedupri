/**
 * \file glade.c
 * \brief fichier qui gère les interactions avec la fenêtre glade, c'est l'interface utilisateur
 *
 * \author tom LARRAUFIE, kevin ZHOU, issam TEBBIKH
 * \version 0.1
 * \date 20/12/2019

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>

#include "clientcxnmanager.h"
#include "featureConfig.h"

#include "featureConfig.h"
#include <gtk/gtk.h>
#include "glade.h"

/**
 * \struct ThreadArg
 * \brief récupère les paramètres du \b GladeBuilder
 */
typedef struct {
    int sockfd;
    GtkBuilder *builderDuel;
} ThreadArg;

GtkBuilder *builderDuel = NULL;
int sockfd;


/**
 * \brief Fonction qui est appellé au clic sur le bouton \b commencer de la fenêtre glade
 * créer la fenêtre de jeu et la cache tant que le duel n'a pas commencé
 * @param win
 */
void on_clickStart(GtkWidget *win){
    
    ThreadArg *args = malloc(sizeof(ThreadArg));
    int status = 0;
    char msg[100];
    pthread_t thread;
    
    //Cache la fentetre 1
    GtkWidget *window = gtk_widget_get_parent(win);
    GtkWidget *window2 = gtk_widget_get_parent(window);
    gtk_widget_hide(window2);
    
    
    //création de la fenetre 2
    GtkWidget * win1;
    builderDuel = gtk_builder_new_from_file("glade/win1.glade");
    win1=GTK_WIDGET(gtk_builder_get_object(builderDuel, "win1"));
    gtk_builder_connect_signals(builderDuel, NULL); //lance la fenetre
    gtk_widget_show(win1);
    
    sockfd = open_connection();
    
    args->sockfd = sockfd;
    args->builderDuel = builderDuel;
    
    Player player= readConfig();
    player.typePacket = 1;
    write(sockfd, &player, sizeof(Player));
    
    //Creation d'un pthread de lecture
    pthread_create(&thread, 0, threadProcess, args);
    //write(connection->sock,"Main APP Still running",15);
    pthread_detach(thread);
    int k=0;
//    do {
//        k++;
//    } while (k>0);
}


/**
 * \brief Fonction qui permet de fermer la fenêtre et le programme quand on clique sur la croix de fermeture de la fenêtre glade
 */
void on_cancel() {

    printf("OK Pressed \n");
    printf("quitting\n ");
    gtk_main_quit();

}


/**
 * \brief Fonction qui se déclenche lors du clic sur le bouton \b VOLER
 * envoi un message de type 4 au serveur
 */
void steal(){
    
    GameDecision *decision = malloc(sizeof(GameDecision));
    decision->typePacket = 4;
    decision->choice = 1;
    write(sockfd, decision, sizeof(GameDecision));

}


/**
 * \brief Fonction qui se déclenche lors du clic sur le bouton \b PARTAGER
 * envoi un message de type 4 au serveur
 */
void share(){
    
    GameDecision *decision = malloc(sizeof(GameDecision));
    decision->typePacket = 4;
    decision->choice = 0;
    write(sockfd, decision, sizeof(GameDecision));
}
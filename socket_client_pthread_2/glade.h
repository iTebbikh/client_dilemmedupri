/**
 * \file glade.h
 * \brief Fichier qui contient les signatures des fonction du fichier glade.c
 *
 * \author tom LARRAUFIE
 * \version 0.1
 * \date 20/12/2019

 */
#ifndef GLADE_H
#define GLADE_H

#ifdef __cplusplus
extern "C" {
#endif




#ifdef __cplusplus
}
#endif


void on_clickSteal();
void on_clickShare();
void on_cancel();


#endif /* GLADE_H */


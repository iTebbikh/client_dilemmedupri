/**
 * \file clientcxnmanager.c
 * \brief fichier d'interaction avec les réponses du serveur
 * \author tom LARRAUFIE, issam TEBBIKH, kevin ZHOU
 * \version 0.1
 * \date 20/12/2019

 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <gtk/gtk.h>
#include "glade.h"

#include "clientcxnmanager.h"
#include "featureConfig.h"
#include "featureConfig.h"

/**
 * \struct ThreadArg
 * \brief récupère les paramètres du \b GladeBuilder
 */
typedef struct {
    int sockfd;
    GtkBuilder *builderDuel;
} ThreadArg;

int cptTimer = 5;

/**
 * \brief fonction StartTimer qui permet d'activier un compte à rebours de 5 secondes
 * @param label1 label qui affiche le compte à rebours sur la page client
 * @return la boucle s'arrete quand la focntion retourne false. 
 */
gboolean StartTimer(gpointer label1) {
    extern GtkBuilder *builderDuel;
    GtkLabel *label_timer = GTK_LABEL(gtk_builder_get_object(builderDuel, "label_info"));
    GtkWidget * btn_steal = GTK_WIDGET(gtk_builder_get_object(builderDuel, "btn_steal"));
    GtkWidget * btn_share = GTK_WIDGET(gtk_builder_get_object(builderDuel, "btn_share"));
    
    char txt[100];

    snprintf(txt, 100, "%2i", cptTimer);

    gtk_label_set_text(label1, txt);
    cptTimer--;

    if (cptTimer < 0) { 
        gtk_widget_set_sensitive (btn_share,TRUE);
        gtk_widget_set_sensitive (btn_steal,TRUE);
        gtk_widget_hide(label_timer);
        
        return false;
        
    } else return true;

}

/**
 * \brief thread qui permet de récupérer les messages du serveur
 * @param ptr
 * @return 
 */
void *threadProcess(void * ptr) {
    char buffer_in[BUFFERSIZE];
    char txt[120];
    Message *msg = malloc(sizeof(Message));
    
    ThreadArg *args = ((ThreadArg *) ptr);
    
    int sockfd = args->sockfd;
    GtkBuilder *builderDuel = args->builderDuel;
    
    GtkWidget * win1 = GTK_WIDGET(gtk_builder_get_object(builderDuel, "win1"));
    GtkLabel *label_info = GTK_LABEL(gtk_builder_get_object(builderDuel, "label_info"));
    GtkLabel *label_timer = GTK_LABEL(gtk_builder_get_object(builderDuel, "label_info"));
    GtkLabel *label_round = GTK_LABEL(gtk_builder_get_object(builderDuel, "label_round"));
    GtkWidget * btn_steal = GTK_WIDGET(gtk_builder_get_object(builderDuel, "btn_steal"));
    GtkWidget * btn_share = GTK_WIDGET(gtk_builder_get_object(builderDuel, "btn_share"));
    
    
    gtk_label_set_markup(label_info, "<span foreground='green' weight='bold' font='12'>Connection success! Searching for opponent</span>");
    gtk_widget_set_sensitive (btn_share,FALSE);
    gtk_widget_set_sensitive (btn_steal,FALSE);
    
    printf("Win type: %s\n", gtk_widget_get_name(win1));

    int len, k=0;
    while ((len = read(sockfd, buffer_in, BUFFERSIZE)) > 0) {
        
        memcpy(msg, buffer_in, sizeof(Message));
        
        /**
         * \brief switch qui permet de s'adapter au type de message envoyer par le serveur selon le TypePacket que le serveur envoi
         * @param ptr
         * @return 
         */
        switch(msg->typePacket){
            case 2: // La connexion est validé, en attente de joeuur
                printf("Message2: %s\n", msg->label);
                break;
                
            case 3: // Duel créer, lancement de la partie
                printf("Message3: %s\n", msg->label);
                gtk_label_set_markup(label_info, "<span foreground='black' weight='bold' font='12'>Le duel va commencer !</span>");

                sleep(1);
                guint timer = g_timeout_add(1000, StartTimer, label_timer);
                break;    
            
            case 5: // choix effectué
                printf("Message5: %s\n", msg->label);
                gtk_widget_show(label_timer);
                gtk_label_set_markup(label_info, "<span foreground='black' weight='bold' font='12'>En attente des resultats...</span>");
                gtk_widget_set_sensitive (btn_share,FALSE);
                gtk_widget_set_sensitive (btn_steal,FALSE);
                break;
                
            case 6: // affichage des resultats
                strcpy(txt, "<span foreground='black' weight='bold' font='12'>");
                strcat(txt, msg->label);
                strcat(txt, "</span>");
                
                gtk_widget_show(label_timer);
                gtk_label_set_markup(label_info, txt);
                gtk_widget_set_sensitive (btn_share,FALSE);
                gtk_widget_set_sensitive (btn_steal,FALSE);
                sleep(3);
                //affiche le résultat du round
                //update du score qu'on affiche?
                printf("Message6: %s\n", msg->label);
                break;
                
            case 7: // Il reste des rounds à lancer
                printf("Message7: %s\n", msg->label);
                gtk_label_set_markup(label_info, "<span foreground='black' weight='bold' font='12'>next round </span>");
                sleep(1);
                timer = g_timeout_add(1000, StartTimer, label_timer);
                break;
                
            case 8: // Game over
                printf("Message8: %s\n", msg->label);
                //Suprimmer tout le contenu de la fenetre
                //Afficher Game Over! Calcul du résultat en cours..
                gtk_widget_destroy(btn_steal);
                gtk_widget_destroy(btn_share);
                gtk_widget_hide(label_round);
                gtk_widget_hide(label_info);
                break;
                
            case 9: // Récupération des structures des deux joueurs
                printf("Message9: %s\n", msg->label);
                //Affiche les résultats
                //gtk_main_quit();
                break;

                
        }

    }
    close(sockfd);
    printf("client pthread ended, len=%d\n", len);
}


/**
 * \brief fonction qui permet d'ouvrir la connexion au client
 * @return 
 */
int open_connection() {
    int sockfd;
    Player player= readConfig();

    struct sockaddr_in serverAddr;
    int port = player.PORT;

    // Create the socket. 
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    //Configure settings of the server address
    // Address family is Internet 
    serverAddr.sin_family = AF_INET;
    //Set port number, using htons function 
    serverAddr.sin_port = htons(port);
    //Set IP address to localhost
    //MODIF :
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    //serverAddr.sin_addr.s_addr = inet_addr(player.IP);

    memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);

    //Connect the socket to the server using the address
    if (connect(sockfd, (struct sockaddr *) &serverAddr, sizeof (serverAddr)) != 0) {
        printf("Fail to connect to server");
        exit(-1);
    };
    printf("Vous êtes connecté!\n");

    return sockfd;
}

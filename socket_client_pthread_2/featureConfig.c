/**
 * \file featureConfig.c
 * \brief fichier qui récupère le fichier de config.txt et qui permet de stocker les valeurs dans des variables
 *
 * \author thomas ROSSI
 * \version 0.1
 * \date 20/12/2019

 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "featureConfig.h"


/**
 * \brief Créer une structure config de type Player qui récupère les paramètres du fichier de configuuration
 * @return 
 */
Player readConfig() {
    Player config;
    
    FILE* fichier = NULL;
    fichier = fopen("config.txt", "r");

    char ligne[30];
    char delim[] = "=";

    for (int i = 0; i < 5; i++) {
        fscanf(fichier, "%s", ligne);
        char *ptr = strtok(ligne, delim);

        if (strcmp(ptr, "NAME") == 0) {
            ptr = strtok(NULL, delim);
            strcpy(config.NAME, ptr);
        } else if (strcmp(ptr, "PORT") == 0) {
            ptr = strtok(NULL, delim);
            config.PORT=atoi(ptr);
        } else if (strcmp(ptr, "IP") == 0) {
            ptr = strtok(NULL, delim);
            strcpy(config.IP, ptr);
        } else if (strcmp(ptr, "KEY") == 0) {
            ptr = strtok(NULL, delim);
            strcpy(config.KEY, ptr);
        } else if (strcmp(ptr, "SCORE") == 0) {
            ptr = strtok(NULL, delim);
            config.score=atoi(ptr);
        }


    }
    fclose(fichier);
    return config;
}

/**
 * Fonction qui permet d'ecrire les résultats dans un fichier "write_file.txt"
 * @param toWrite
 */
void writeResult(ToWrite toWrite) {
    FILE *fichier = fopen("write_file.txt", "w");
    fprintf(fichier, "%s=%s", "Nom", toWrite.nom);
    fputs("\n",fichier);
    fprintf(fichier, "%s=%s", "Prenom", toWrite.prenom);
    fputs("\n",fichier);
    fprintf(fichier, "%s=%i", "Score", toWrite.score);
    fputs("\n",fichier);

    fclose(fichier);
}
//int main() {
//    Config config = readConfig();
//    printf("L'IP est %s\n", config.IP);
//    printf("Le PORT est %s\n", config.PORT);
//    printf("La clée est %s\n", config.KEY);
//    
//    ToWrite toWrite;
//    strcpy(toWrite.nom , "ROSSI");
//    strcpy(toWrite.prenom , "Thomas");
//    toWrite.score = 13;
//    write(toWrite);
//}
/**
 * \file featureConfig.h
 * \brief fichier d'entete qui contient les structures necessaire a la communication avec le serveur
 *
 * \author tom LARRAUFIE, kevin ZHOU, issam TEBBIKH
 * \version 0.1
 * \date 20/12/2019

 */


#ifndef FEATURECONFIG_H
#define FEATURECONFIG_H


/**
 * \struct Player
 * \brief contient toutes les données d'un joueur
 */
typedef struct {
    int typePacket;
    char NAME[16];
    char IP[16];
    char KEY[10];
    int PORT;
    int score;
    int status; //1=en jeu , 0=pas en jeu
    int choix;
    int index;

} Player;


/**
 * \struct ToWrite
 */
typedef struct {
    char nom[16];
    char prenom[5];
    int score;

} ToWrite;


Player readConfig();
void writeResult(ToWrite toWrite);



#endif /* FEATURECONFIG_H */


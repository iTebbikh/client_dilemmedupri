/**
 * \file clientcxnmanager.h
 * \author tom LARRAUFIE, kevin ZHOU, issam TEBBIKH
 * \version 0.1
 * \date 20/12/2019

 */

#ifndef CLIENTCXNMANAGER_H
#define CLIENTCXNMANAGER_H

#define BUFFERSIZE 2048
#include <stdbool.h>

/**
 * \struct Message
 * \brief structure qui permet au client de recevoir les données envoyé par le serveur
 */
typedef struct {
    int typePacket; // Savoir le type du message (start ou getready...)
    char label[120];
    int status; // Message reçu ou pas
     
}Message;


/**
 * \struct GameDecision
 * \brief structure qui permet au client de recevoir les choix des joueurs envoyé par le serveur
 * 
 */
typedef struct {
    int typePacket; // Savoir le type du message (start ou getready...)
    int choice; // Choix du joueur 1=voler , 0=partager
     
}GameDecision;

void *threadProcess(void * ptr);
int open_connection();

#endif /* CLIENTCXNMANAGER_H */

